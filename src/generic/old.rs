use std;
use tinyvec::TinyVec;
use std::marker::PhantomData;
use super::bbll::IndentDepth;
use super::bbll::IndentLength;

pub trait IndentMeasure {
    /// A measure of the indentation of a line, relative to a line that is not indented
    type Depth;

    /// A measure of the difference between two levels of indentation
    ///
    /// This is an absolute (unsigned) difference, i.e., it could as well be a decrease as an
    /// increase in indentation.
    type Delta;
}

pub struct UnitMeasure;

impl IndentMeasure for UnitMeasure {
    type Depth = IndentDepth;
    type Delta = IndentLength;
}

/// A token
pub enum Token<T, M> where M: IndentMeasure {
    /// A non-indentation token
    Content(T),

    /// An increase in indentation depth
    Indent(M::Delta),

    /// A decrease in indentation depth
    Deindent(M::Delta),
}

/// Defines how each character at the start of a line is processed to determine the line's
/// indentation level
pub enum IndentItemInterpretation<M> where M:IndentMeasure {
    /// The presence of the character means that the indentation deepens by the given amount
    /// (relative to a line that is not indented).
    Indent(M::Delta),

    /// The character encountered is not an indentation character; the part of the line that is not
    /// indentation has been reached.
    NotIndent,

    /// The character encountered is assumed to be intended as an indentation character, but it is
    /// not allowed to be used as one.
    BadIndent,
}

pub enum InputBufferInterpretation<T, E> {
    Incomplete,

    Ok(T),

    Err(E),
}

/// An iterator over `Token`s
pub struct Tokens<LexPol, Input> where Input: Iterator, LexPol: Policy<InputItem = Input::Item> {
    input: Input,
    lex_policy: LexPol,
    depth_prev: LexPol::IndentMeasure::Depth,
    next_content_item: Option<Input::Item>,
    input_buffer: LexPol::InputBuffer,
}

impl<LexPol, Input> Iterator for Tokens<LexPol, Input> where Input: Iterator<Item = u8>, LexPol: Policy<InputItem = Input::Item, IndentMeasure = ByUnit> {
    type Item = Token<Input::Item, ByUnit>;

    fn next(&mut self) -> Option<Self::Item> {
        type M = ByUnit;

        let Self {input, lex_policy, prev_line_indent, next_content_item, input_buffer} = self;

        debug_assert!(input_buffer.is_empty());

        match next_content_item.take() {
            Some(item) => return Some(item),
            None => {}
        }

        let mut depth_here = M::Depth(0);

        for input_item in input {
            input_buffer.extend(iter::once(input_item));
            match policy.interpret_input_char(input_item) {
                IndentCharInterpretation::Indent(M::Delta(delta)) => depth_here.0 += delta,
                IndentCharInterpretation::NotIndent => {
                    if 
                    return Some(Token::Content(input_item))
                }
                IndentCharInterpretation::BadIndent => todo!(),
            }
        }

        None
    }
}

/// A type implementing this trait defines how indentation is lexed.
pub trait IndentPolicy {
    /// The type of item expected from the input iterator
    type InputItem;

    /// The type of item that we will accept
    type IntakeItem;

    type IndentMeasure: IndentMeasure;

    /// A buffer used to store input items until they can be parsed into an intake item
    ///
    /// If there is a small upper bound on the number of input items that could need to be read to
    /// get one intake item (e.g., four `u8` values to make one `char` value), this can be set for
    /// efficiency to a fixed-length buffer (e.g., `tinyvec::ArrayVec`).
    type InputBuffer: AsRef<[Self::InputItem]> + Extend<Self::InputItem> = Vec<Self::InputItem>;

    fn interpret_input_buffer(&mut self, buffer: &[Self::InputItem]) -> InputBufferInterpretation;

    /// Defines how each character at the start of a line is processed to determine the line's
    /// indentation level
    fn interpret_indent_char(&mut self, c: Self::IntakeItem) -> IndentCharInterpretation;

    /// Defines how the indentation state should change if a sequence of one or more blank lines in
    /// the input (a "gap") is encountered
    ///
    /// `prev_state` is the indentation state before the gap. `gap_length` is the number of blank
    /// lines that constitute the gap. The return value is the indentation state after the gap.
    ///
    /// By default, the indentation state is not changed if a gap is encountered.
    fn interpret_gap(&mut self, prev_state: Self::IndentMeasure::Depth, gap_length: usize) -> Self::IndentMeasure::Depth {
        let _ = gap_length;
        prev_state
    }
}
