use std::fmt;

/// A measure of how deeply a line is indented, relative to a line that is not indented
pub trait IndentDepth: Clone + fmt::Debug + Eq {
    /// A measure of the (signed) difference between two levels of indentation
    type Delta: IndentDelta;

    /// An unsigned (necessarily positive) measure of the difference between two levels of
    /// indentation
    type PositiveDelta;

    /// Computes the unsigned amount by which this indentation depth is deeper than the given
    /// indentation depth, i.e., the distance by which indentation at this depth extends beyond the
    /// given depth
    ///
    /// If `self` is not deeper than `other` (i.e., has no _depth beyond_ that of `other`), `None`
    /// is to be returned.
    fn depth_beyond(&self, other: &Self) -> Option<Self::PositiveDelta>;

    /// Computes the signed amount by which this indentation depth is deeper than the given
    /// indentation depth, i.e., the signed distance by which indentation at this depth extends
    /// beyond the given depth
    ///
    /// If `self` and `other` are equally deep (i.e., there is no _delta_ between them), `None` is
    /// to be returned.
    fn delta_versus(&self, other: &Self) -> Option<Self::Delta>;
}

/// A measure of the difference ("delta") between two levels of indentation
///
/// This must be a signed difference, i.e., it must distinguish an increase from a decrease in
/// indentation.
///
/// This type should not be able to represent no change in indentation, i.e., it should use
/// [`NonZeroUsize`] or similar.
///
/// [`NonZeroUsize`]: <https://doc.rust-lang.org/std/num/struct.NonZeroUsize.html>
pub trait IndentDelta: Clone + fmt::Debug + Eq {}

/// A trait for types that can be interpreted sensibly as having a numerical value
///
/// The term "numerical value" is taken from metrology for, of a quantity that usually would be
/// given with a unit of measurement, the part that remains if the unit is stripped off; see, e.g.,
/// guides published by the U.S. National Institute of Standards and Technology concerning the use
/// of the International System of Units.
pub trait NumericalValue<N>: From<N> {
    fn numerical_value(&self) -> N;
}

/// A token that represents either an underlying "content" token or a change ("delta") in
/// indentation level, with the latter represented by a single token containing a numerical measure
/// of the change in indentation level
#[derive(Clone, Debug, Eq)]
pub enum DeltaicToken<T, M>
where
    M: IndentDepth,
    T: Clone + fmt::Debug + Eq + PartialEq,
{
    /// A non-indentation token
    Content(T),

    /// A change in indentation depth
    IndentChange(M::Delta),
}

/// A token that represents either an underlying "content" token or a change (or part of a change)
/// in indentation level, with changes in indentation level represented by sequences of one or more
/// such tokens, with one such token for each unit of indentation (e.g., space) by which the
/// indentation level is changed
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum UnitalToken<T>
where
    T: Clone + fmt::Debug + Eq + PartialEq,
{
    /// A non-indentation token
    Content(T),

    /// An increase in indentation depth by a unit of indentation (e.g., a space)
    Indent,

    /// A decrease in indentation depth by a unit of indentation (e.g., a space)
    Deindent,
}

// [2020-07-04] Apparently `#[derive(PartialEq)]` works only if all the type parameters to `Self`
// implement `PartialEq`, even if those types don't actually appear in `Self` (in my case, because
// `M` is used only for its associated types).
impl<T, M> PartialEq for DeltaicToken<T, M>
where
    M: IndentDepth,
    T: Clone + fmt::Debug + Eq + PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Content(c1), Self::Content(c2)) => c1 == c2,
            (Self::IndentChange(i1), Self::IndentChange(i2)) => i1 == i2,
            _ => false,
        }
    }
}

/*
/// Defines how each character at the start of a line is processed to determine the line's
/// indentation level
pub enum IndentItemInterpretation<M> where M:IndentMeasure {
    /// The presence of the character means that the indentation deepens by the given amount
    /// (relative to a line that is not indented).
    Indent(M::Delta),

    /// The character encountered is not an indentation character; the part of the line that is not
    /// indentation has been reached.
    NotIndent,

    /// The character encountered is assumed to be intended as an indentation character, but it is
    /// not allowed to be used as one.
    BadIndent,
}

pub enum InputBufferInterpretation<T, E> {
    Incomplete,

    Ok(T),

    Err(E),
}

/// A type implementing this trait defines how indentation is lexed.
pub trait IndentPolicy {
    /// The type of item expected from the input iterator
    type InputItem;

    /// The type of item that we will accept
    type IntakeItem;

    type IndentMeasure: IndentMeasure;

    /// A buffer used to store input items until they can be parsed into an intake item
    ///
    /// If there is a small upper bound on the number of input items that could need to be read to
    /// get one intake item (e.g., four `u8` values to make one `char` value), this can be set for
    /// efficiency to a fixed-length buffer (e.g., `tinyvec::ArrayVec`).
    type InputBuffer: AsRef<[Self::InputItem]> + Extend<Self::InputItem>; //= Vec<Self::InputItem>;

    fn interpret_input_buffer(&mut self, buffer: &[Self::InputItem]) -> InputBufferInterpretation;

    /// Defines how each character at the start of a line is processed to determine the line's
    /// indentation level
    fn interpret_indent_char(&mut self, c: Self::IntakeItem) -> IndentItemInterpretation;

    /// Defines how the indentation state should change if a sequence of one or more blank lines in
    /// the input (a "gap") is encountered
    ///
    /// `prev_state` is the indentation state before the gap. `gap_length` is the number of blank
    /// lines that constitute the gap. The return value is the indentation state after the gap.
    ///
    /// By default, the indentation state is not changed if a gap is encountered.
    fn interpret_gap(&mut self, prev_state: Self::IndentMeasure::Depth, gap_length: usize) -> Self::IndentMeasure::Depth {
        let _ = gap_length;
        prev_state
    }
}
*/

// Vim: set spell:
