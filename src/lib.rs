//#![forbid(missing_docs)]
#![forbid(unsafe_code)]
#![warn(clippy::all)]
#![warn(clippy::pedantic)]

#[macro_use]
extern crate derivative;

pub mod basic;
//pub mod bbll;
pub mod generic;
pub mod homogeneous;

// Vim: set spell:
