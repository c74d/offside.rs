//! Low-level lexing of indentation, counting by atomic units of indentation, such as individual
//! spaces, rather than by indentation levels

use super::generic::UnitMeasure;
use super::generic;
use std::iter::Peekable;
use std::iter::FusedIterator;
use combine::Stream;
use combine::RangeStream;

pub type Token<T> = generic::Token<T, UnitMeasure>;

/// A difference between two indentation levels, measured in individual, atomic units of
/// indentation, such as individual spaces
///
/// This is an absolute (unsigned) difference, i.e., it could as well be a decrease as an increase
/// in indentation.
pub struct IndentLength(pub usize);

/// The `IndentLength` between a given line's indentation level and the indentation level of a line
/// that is not indented
pub struct IndentDepth(pub IndentLength);

/// An iterator over `Token`s
pub struct Tokens<Input, IndPol, LinBrkPol, OutputAdapter, InputAdapter> where Input: Stream, InputAdapter: Adapter<InputItem = Input::Token>, OutputAdapter: Adapter<InputItem = InputAdapter::OutputItem>, IndPol: IndentPolicy<InputItem = InputAdapter::OutputItem>, LinBrkPol: LineBreakPolicy<InputItem = InputAdapter::OutputItem> {
    input: Input,

    input_adapter: InputAdapter,

    depth_prev: IndentDepth,

    indent_policy: IndPol,

    line_break_policy: LinBrkPol,

    /// Are we at the "content" of a line?  I.e., are we past the indentation?
    at_line_content: bool,

    output_adapter: OutputAdapter,
}

impl<IndPol, LinBrkPol, Input> StreamOnce for Tokens<Input, IndPol, LinBrkPol, OutputAdapter, InputAdapter> where Input: Stream, InputAdapter: Adapter<InputItem = Input::Token>, OutputAdapter: Adapter<InputItem = InputAdapter::OutputItem>, IndPol: IndentPolicy<InputItem = InputAdapter::OutputItem>, LinBrkPol: LineBreakPolicy<InputItem = InputAdapter::OutputItem> {
    type Token = Token<Input::Token>;

    type Range = Self::Token;

    type Position = Input::Position;

    type Error = Input::Error;

    fn next(&mut self) -> Option<Self::Token> {
        type M = UnitMeasure;

        let Self {input, depth_prev, indent_policy, line_break_policy, at_line_content} = self;

        if at_line_content {
            line_break_policy._line_break
            return input.next().map(Token::Content);
        }

        // We are at the start of a new line.

        let mut depth_here = M::Depth(0);

        for input_item in input {
            match lex_policy.interpret_input(&input_item) {
                Indent(M::Delta(delta)) => {
                    depth_here.0 += delta
                }
                Content => {
                    at_line_content = true;

                    let content_token = Token::Content(input_item);

                    let (output_now, output_delayed) = match (depth_here.0.checked_sub(depth_prev.0), depth_prev.0.checked_sub(depth_here.0)) {
                        (Some(0), Some(0)) =>
                            (content_token, None),
                        (Some(depth_increase), None) =>
                            (Token::Indent(M::Delta(depth_increase)), Some(content_token)),
                        (None, Some(depth_decrease)) =>
                            (Token::Deindent(M::Delta(depth_decrease)), Some(content_token)),
                        (None, None) => unreachable!(),
                    };

                    debug_assert_eq!(next_output_item, None);
                    next_output_item = output_delayed;

                    return Some(output_now);
                }
            }
        }

        None
    }
}

// Vim: set spell:
