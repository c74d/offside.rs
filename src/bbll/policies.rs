//! Ready-to-use indentation lexing policies, i.e., implementers of `Policy`

use super::Policy;

/// The atomic unit of indentation is a single U+0020 SPACE character, and U+0009 CHARACTER
/// TABULATION ("tab") is forbidden as an indentation character.
pub struct Space;

/// The atomic unit of indentation is a single U+0009 CHARACTER TABULATION ("tab") character, and
/// U+0020 SPACE is forbidden as an indentation character.
pub struct Tab;
