//! Low-level lexing of indentation, counting by individual spaces

use super::generic;
use super::generic::IndentDepth as _;
use crate::generic::NumericalValue;
use crate::homogeneous;
use crate::homogeneous::IndentDeltaNumericalValue;
use std::fmt;
use std::iter::Fuse;
use std::mem;
use std::num::NonZeroUsize;

pub type DeltaicToken<T> = generic::DeltaicToken<T, IndentDepth>;

pub type UnitalToken<T> = generic::UnitalToken<T>;

type HDepth = homogeneous::IndentDepth<homogeneous::measures::Null<()>>;

type HDelta = homogeneous::IndentDelta<homogeneous::measures::Null<()>>;

/// A difference between two indentation levels, measured in individual spaces
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum IndentDelta {
    Indent(NonZeroUsize),
    Deindent(NonZeroUsize),
}

/// The difference between a given line's indentation level and the indentation level of a line
/// that is not indented, measured in individual spaces
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct IndentDepth(usize);

/// An iterator over `UnitalToken`s
///
/// # Examples
///
/// ```rust
/// # extern crate offside;
/// # use offside::basic::UnitalTokens;
/// # use offside::basic::UnitalToken;
/// # fn main() {
/// let input = br"A
///   B";
///
/// let mut tokens = UnitalTokens::from(input.iter().cloned());
///
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'A')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'B')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), None);
///
/// let input = br"A
///   B
///      C";
///
/// let mut tokens = UnitalTokens::from(input.iter().cloned());
///
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'A')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'B')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'C')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), None);
///
/// let input = br"
///   A:
///      1+2
///      =3
///   B;
///     ?
///    !
/// ";
///
/// let mut tokens = UnitalTokens::from(input.iter().cloned());
///
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'A')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b':')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'1')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'+')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'2')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'=')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'3')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'B')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b';')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Indent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'?')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'!')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), Some(UnitalToken::Deindent));
/// assert_eq!(tokens.next(), None);
/// # }
/// ```
pub struct UnitalTokens<Input>
where
    Input: Iterator<Item = u8>,
{
    deltaic_tokens: DeltaicTokens<Input>,
    delayed_output: Option<IndentDelta>,
}

impl<Input> From<Input> for UnitalTokens<Input::IntoIter>
where
    Input: IntoIterator<Item = u8>,
{
    fn from(src: Input) -> Self {
        Self {
            deltaic_tokens: src.into(),
            delayed_output: None,
        }
    }
}

impl<Input> Iterator for UnitalTokens<Input>
where
    Input: Iterator<Item = u8>,
{
    type Item = UnitalToken<Input::Item>;

    fn next(&mut self) -> Option<Self::Item> {
        type Token<T> = UnitalToken<T>;

        let Self {
            deltaic_tokens,
            delayed_output,
        } = self;

        if let Some((immediate_output, new_delayed_output)) =
            delayed_output.take().map(IndentDelta::split_first_unital)
        {
            *delayed_output = new_delayed_output;
            return Some(immediate_output);
        }

        let (immediate_output, new_delayed_output) = match {
            debug_assert_eq!(delayed_output, &None);
            // There is no delayed output left, so it's okay to return early by bubbling up a
            // `None` if we get one.
            deltaic_tokens.next()?
        } {
            DeltaicToken::Content(content_item) => (Token::Content(content_item), None),
            DeltaicToken::IndentChange(delta) => delta.split_first_unital(),
        };

        debug_assert_eq!(delayed_output, &None);
        *delayed_output = new_delayed_output;

        Some(immediate_output)
    }
}

/// An iterator over `DeltaicToken`s
///
/// At the start of each line, this iterator scans to the first non-indentation character to
/// compute how much indentation the line has and then, if that amount of indentation differs from
/// the preceding line's, yields a single token representing the change as some kind of number.
///
/// This may make a less convenient base atop which to write a parser than would [`UnitalTokens`],
/// but this provides a base atop which to write [`UnitalTokens`] itself, as, to determine a line's
/// indentation depth, it is necessary anyway to scan from the start of the line to the end of its
/// indentation.
///
/// [`UnitalTokens`]: <struct.UnitalTokens.html>
///
/// # Examples
///
/// ```rust
/// # extern crate offside;
/// # use offside::basic::DeltaicTokens;
/// # use offside::basic::DeltaicToken;
/// # use offside::basic::IndentDelta;
/// use std::num::NonZeroUsize;
///
/// # fn main() {
/// let indent = |size| IndentDelta::Indent(NonZeroUsize::new(size).unwrap());
/// let deindent = |size| IndentDelta::Deindent(NonZeroUsize::new(size).unwrap());
///
/// let input = br"A
///   B";
///
/// let mut tokens = DeltaicTokens::from(input.iter().cloned());
///
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'A')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(indent(2))));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'B')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(deindent(2))));
/// assert_eq!(tokens.next(), None);
///
/// let input = br"A
///   B
///      C";
///
/// let mut tokens = DeltaicTokens::from(input.iter().cloned());
///
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'A')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(indent(2))));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'B')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(indent(3))));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'C')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(deindent(5))));
/// assert_eq!(tokens.next(), None);
///
/// let input = br"
///   A:
///      1+2
///      =3
///   B;
///     ?
///    !
/// ";
///
/// let mut tokens = DeltaicTokens::from(input.iter().cloned());
///
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(indent(2))));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'A')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b':')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(indent(3))));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'1')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'+')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'2')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'=')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'3')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(deindent(3))));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'B')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b';')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(indent(2))));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'?')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(deindent(1))));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'!')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::Content(b'\n')));
/// assert_eq!(tokens.next(), Some(DeltaicToken::IndentChange(deindent(3))));
/// assert_eq!(tokens.next(), None);
/// # }
/// ```
pub struct DeltaicTokens<Input>
where
    Input: Iterator<Item = u8>,
{
    input: Fuse<Input>,

    depth_prev: IndentDepth,

    next_output_item: Option<DeltaicToken<Input::Item>>,

    /// Are we at the "content" of a line?  I.e., are we past the indentation?
    at_line_content: bool,
}

impl<Input> From<Input> for DeltaicTokens<Input::IntoIter>
where
    Input: IntoIterator<Item = u8>,
{
    fn from(src: Input) -> Self {
        Self {
            input: src.into_iter().fuse(),
            depth_prev: IndentDepth(0),
            next_output_item: None,
            at_line_content: false,
        }
    }
}

impl<Input> Iterator for DeltaicTokens<Input>
where
    Input: Iterator<Item = u8>,
{
    type Item = DeltaicToken<Input::Item>;

    fn next(&mut self) -> Option<Self::Item> {
        type Token<T> = DeltaicToken<T>;

        let handle_end_of_input = |IndentDepth(depth_prev): &mut _| -> Option<Self::Item> {
            Some(Token::IndentChange(IndentDelta::Deindent(
                NonZeroUsize::new(mem::take(depth_prev))?,
            )))
        };

        let Self {
            input,
            depth_prev,
            next_output_item,
            at_line_content,
        } = self;

        if let Some(output_item) = next_output_item.take() {
            return Some(output_item);
        }

        if *at_line_content {
            return match input.next() {
                Some(input_item) => {
                    if input_item == b'\n' {
                        *at_line_content = false;
                    }
                    Some(Token::Content(input_item))
                }
                None => handle_end_of_input(depth_prev),
            };
        }

        // We are at the start of a new line.

        let mut depth_here = IndentDepth(0);

        for input_item in input {
            debug_assert!(!*at_line_content);

            match input_item {
                b' ' => {
                    depth_here.0 = depth_here
                        .0
                        .checked_add(1)
                        // TODO: Return an error properly.
                        .expect("indentation depth overflowed pointer-sized unsigned integer");
                }
                b'\n' => {
                    return Some(Token::Content(input_item));
                }
                _ => {
                    *at_line_content = true;

                    let content_token = Token::Content(input_item);

                    let (output_now, output_delayed) = match depth_here.delta_versus(depth_prev) {
                        None => (content_token, None),
                        Some(delta) => (Token::IndentChange(delta), Some(content_token)),
                    };

                    debug_assert_eq!(*next_output_item, None);
                    *next_output_item = output_delayed;

                    *depth_prev = depth_here;

                    return Some(output_now);
                }
            }
        }

        handle_end_of_input(depth_prev)
    }
}

impl generic::IndentDepth for IndentDepth {
    type Delta = IndentDelta;

    type PositiveDelta = NonZeroUsize;

    fn depth_beyond(&self, other: &Self) -> Option<Self::PositiveDelta> {
        HDepth::from(self.numerical_value()).depth_beyond(&other.numerical_value().into())
    }

    fn delta_versus(&self, other: &Self) -> Option<Self::Delta> {
        HDepth::from(self.numerical_value())
            .delta_versus(&other.numerical_value().into())
            .map(|d| d.numerical_value().into())
    }
}

impl NumericalValue<usize> for IndentDepth {
    fn numerical_value(&self) -> usize {
        let &Self(depth) = self;
        depth
    }
}

impl From<usize> for IndentDepth {
    fn from(numerical_value: usize) -> Self {
        Self(numerical_value)
    }
}

impl IndentDelta {
    fn split_first_unital<T>(self) -> (UnitalToken<T>, Option<Self>)
    where
        T: Clone + fmt::Debug + Eq + PartialEq,
    {
        let (token, rest) = HDelta::from(self.numerical_value()).split_first_unital();
        (token, rest.map(|d| d.numerical_value().into()))
    }
}

impl generic::IndentDelta for IndentDelta {}

impl NumericalValue<IndentDeltaNumericalValue> for IndentDelta {
    fn numerical_value(&self) -> IndentDeltaNumericalValue {
        match self {
            &Self::Indent(size) => IndentDeltaNumericalValue::Indent(size),
            &Self::Deindent(size) => IndentDeltaNumericalValue::Deindent(size),
        }
    }
}

impl From<IndentDeltaNumericalValue> for IndentDelta {
    fn from(numerical_value: IndentDeltaNumericalValue) -> Self {
        match numerical_value {
            IndentDeltaNumericalValue::Indent(size) => Self::Indent(size),
            IndentDeltaNumericalValue::Deindent(size) => Self::Deindent(size),
        }
    }
}

// Vim: set spell:
