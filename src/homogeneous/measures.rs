use crate::homogeneous::IndentMeasure;
use std::convert::TryInto;
use std::marker::PhantomData;
use std::mem;
use std::num::NonZeroUsize;

/// An indentation measure that never considers any character to represent indentation
#[derive(Debug)]
pub struct Null<C>(PhantomData<fn(C)>);

impl<C> IndentMeasure for Null<C> {
    type InputToken = C;

    fn interpret_input<I>(mut input: I) -> Option<NonZeroUsize>
    where
        I: Iterator<Item = C>,
    {
        mem::drop(input.next());
        None
    }
}

/// An indentation measure that measures by U+0020 SPACE characters ("spaces"), considering no
/// other characters to represent indentation
#[derive(Debug)]
pub struct Space<C>(PhantomData<fn(C)>)
where
    C: TryInto<char>;

impl<C> IndentMeasure for Space<C>
where
    C: TryInto<char>,
{
    type InputToken = C;

    fn unit_description() -> &'static str {
        "space(s)"
    }

    fn interpret_input<I>(mut input: I) -> Option<NonZeroUsize>
    where
        I: Iterator<Item = C>,
    {
        match input.next().map(TryInto::try_into) {
            Some(Ok(' ')) => NonZeroUsize::new(1),
            _ => None,
        }
    }
}

/// An indentation measure that measures by U+0009 CHARACTER TABULATION characters ("tabs"),
/// considering no other characters to represent indentation
#[derive(Debug)]
pub struct Tab<C>(PhantomData<fn(C)>)
where
    C: TryInto<char>;

impl<C> IndentMeasure for Tab<C>
where
    C: TryInto<char>,
{
    type InputToken = C;

    fn unit_description() -> &'static str {
        "tab(s)"
    }

    fn interpret_input<I>(mut input: I) -> Option<NonZeroUsize>
    where
        I: Iterator<Item = C>,
    {
        match input.next().map(TryInto::try_into) {
            Some(Ok('\t')) => NonZeroUsize::new(1),
            _ => None,
        }
    }
}

/// An indentation measure that measures by spaces, treats a tab as eight spaces, and considers no
/// other characters to represent indentation
///
/// This is included mainly as an example to show that this can be done.
#[derive(Debug)]
pub struct Tab8Space<C>(PhantomData<fn(C)>)
where
    C: TryInto<char>;

impl<C> IndentMeasure for Tab8Space<C>
where
    C: TryInto<char>,
{
    type InputToken = C;

    fn unit_description() -> &'static str {
        "space(s) (counting a tab as 8 spaces)"
    }

    fn interpret_input<I>(mut input: I) -> Option<NonZeroUsize>
    where
        I: Iterator<Item = C>,
    {
        match input.next().map(TryInto::try_into) {
            Some(Ok('\t')) => NonZeroUsize::new(8),
            Some(Ok(' ')) => NonZeroUsize::new(1),
            _ => None,
        }
    }
}
