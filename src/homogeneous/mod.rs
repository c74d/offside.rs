use crate::generic;
use crate::generic::NumericalValue;
use crate::generic::UnitalToken;
use std::fmt;
use std::marker::PhantomData;
use std::num::NonZeroUsize;

pub mod measures;

/// A "homogeneous" manner of measuring the indentation depth of a line of source code
///
/// # Definition of homogeneity
///
/// By "homogeneous" is meant that the measurement must be such that the indentation depth can be
/// represented as a product of a numerical value and a unit of measurement, such as "20 spaces" or
/// "3 tabs" — similarly to how a measurement of physical distance could be represented as, taking
/// for example some arbitrary distances, "20 meters" or "3 miles" — and the indentation measure
/// must satisfy the following criteria (which may be, but are not guaranteed to be, partly or
/// wholly redundant with the foregoing):
///
///   1.  All indentation must be represented, or at least representable, using a single, known
///       unit of measurement (as if the indentation itself were _homogeneous_ in its constituent
///       characters, whence we take the word).  E.g., if that unit is a U+0020 SPACE character but
///       tab characters (U+0009 CHARACTER TABULATION) also are allowed as indentation, it must be
///       the case that each tab represents (i.e., is "worth") a certain number of spaces.
///
///   2.  If multiple types of indentation (e.g., spaces and tabs) are allowed to be mixed, then
///       there must be a constant conversion rate between each two kinds of indentation (e.g.,
///       every tab must be "worth" the same number of spaces).
///
///   3.  More generally, the "worth" of indentation characters, i.e., their contribution to the
///       indentation depth, must be constant over each type of indentation; i.e., the "worth" of
///       each indentation character must be the same as that of each other indentation character
///       of the same type.
///
///       1.  In particular, the "worth" of an indentation character must not depend on its
///           position in a file, e.g., its position relative to any other indentation character or
///           to any given syntactic construct.
pub trait IndentMeasure {
    type InputToken;

    fn interpret_input<I>(input: I) -> Option<NonZeroUsize>
    where
        I: Iterator<Item = Self::InputToken>;

    #[must_use = "Getting the description but not using it is most likely a mistake."]
    fn unit_description() -> &'static str {
        "unit(s) of indentation"
    }
}

/// A homogeneous measure of the indentation depth of a line, relative to a line that is not
/// indented
#[derive(Derivative)]
#[derivative(Clone, Copy, Debug, Eq, PartialEq)]
pub struct IndentDepth<M>
where
    M: IndentMeasure,
{
    numerical_value: usize,
    _measure: PhantomData<*mut M>,
}

impl<M> IndentDepth<M> where M: IndentMeasure {}

impl<M> NumericalValue<usize> for IndentDepth<M>
where
    M: IndentMeasure,
{
    /// Returns the indentation depth as a `usize` representing the number of units of indentation
    /// by which lines at this indentation depth are indented
    ///
    /// # Examples
    ///
    /// TODO: Add an example of roundtripping through From
    fn numerical_value(&self) -> usize {
        self.numerical_value
    }
}

impl<M> From<usize> for IndentDepth<M>
where
    M: IndentMeasure,
{
    /// Constructs an indentation depth from a `usize` representing the number of units of
    /// indentation by which lines at this indentation depth are indented
    fn from(numerical_value: usize) -> Self {
        Self {
            numerical_value,
            _measure: PhantomData,
        }
    }
}

impl<M> generic::IndentDepth for IndentDepth<M>
where
    M: IndentMeasure,
{
    type Delta = IndentDelta<M>;

    type PositiveDelta = NonZeroUsize;

    /// Computes the number of units of indentation by which this indentation depth is deeper than
    /// the given indentation depth, i.e., the distance by which indentation at this depth extends
    /// beyond the given depth
    ///
    /// If `self` is not deeper than `other` (i.e., has no _depth beyond_ that of `other`), `None`
    /// is returned.
    fn depth_beyond(&self, other: &Self) -> Option<Self::PositiveDelta> {
        self.numerical_value()
            .checked_sub(other.numerical_value())
            .and_then(NonZeroUsize::new)
    }

    /// Computes the signed number of units of indentation by which this indentation depth is
    /// deeper than the given indentation depth, i.e., the signed distance by which indentation at
    /// this depth extends beyond the given depth
    ///
    /// If `self` and `other` are equally deep (i.e., there is no _delta_ between them), `None` is
    /// returned.
    fn delta_versus(&self, other: &Self) -> Option<Self::Delta> {
        type N = IndentDeltaNumericalValue;

        match (self.depth_beyond(other), other.depth_beyond(self)) {
            (None, None) => None,

            (Some(depth_increase), None) => Some(N::Indent(depth_increase).into()),

            (None, Some(depth_decrease)) => Some(N::Deindent(depth_decrease).into()),

            // It should not be possible for `self` to be deeper than `other` and `other` to be
            // deeper than `self` simultaneously.  This branch should be reached only because of
            // memory corruption, other hardware error, or a mistake in `depth_beyond`.
            (Some(_), Some(_)) => unreachable!(),
        }
    }
}

/// A homogeneous measure of the difference ("delta") between two levels of indentation
#[derive(Derivative)]
#[derivative(Clone, Copy, Debug, Eq, PartialEq)]
pub struct IndentDelta<M>
where
    M: IndentMeasure,
{
    numerical_value: IndentDeltaNumericalValue,
    _measure: PhantomData<*mut M>,
}

impl<M> IndentDelta<M>
where
    M: IndentMeasure,
{
    /// Splits a `UnitalToken::Indent` or `UnitalToken::Deindent` token off of this indentation
    /// delta and returns the token and the remaining delta (if any)
    ///
    /// This method is named after the [`split_first`] method of slices, the idea being that a
    /// homogeneous indentation delta is isomorphic to a finite sequence of identical
    /// `UnitalToken::Indent` or `UnitalToken::Deindent` tokens.
    ///
    /// [`split_first`]: <https://doc.rust-lang.org/std/primitive.slice.html#method.split_first>
    #[must_use = "This method has no side-effects, so using its return value is the only reason to call it."]
    pub fn split_first_unital<T>(self) -> (UnitalToken<T>, Option<Self>)
    where
        T: Clone + fmt::Debug + Eq + PartialEq,
    {
        type N = IndentDeltaNumericalValue;
        type F = fn(NonZeroUsize) -> N;

        let Self {
            numerical_value,
            _measure,
        } = self;

        let (abs_delta, token, rest_constructor): (_, _, F) = match numerical_value {
            N::Indent(d) => (d, UnitalToken::Indent, N::Indent),
            N::Deindent(d) => (d, UnitalToken::Deindent, N::Deindent),
        };

        (
            token,
            abs_delta
                .get()
                .checked_sub(1)
                .and_then(NonZeroUsize::new)
                .map(rest_constructor)
                .map(Into::into),
        )
    }
}

impl<M> NumericalValue<IndentDeltaNumericalValue> for IndentDelta<M>
where
    M: IndentMeasure,
{
    fn numerical_value(&self) -> IndentDeltaNumericalValue {
        self.numerical_value
    }
}

impl<M> From<IndentDeltaNumericalValue> for IndentDelta<M>
where
    M: IndentMeasure,
{
    fn from(numerical_value: IndentDeltaNumericalValue) -> Self {
        Self {
            numerical_value,
            _measure: PhantomData,
        }
    }
}

impl<M> generic::IndentDelta for IndentDelta<M> where M: IndentMeasure {}

/// A type that captures the direction (indent vs de-indent) and number of units of indentation
/// that an indentation delta represents but not the nature of the unit by which the number is
/// measured
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum IndentDeltaNumericalValue {
    Indent(NonZeroUsize),

    Deindent(NonZeroUsize),
}

// Vim: set spell:
